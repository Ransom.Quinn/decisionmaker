var brackets = [];
var loserList = [];
var winnerList = [];
var elimationList = [];
var selectedButton = null;
var notSelectedButton = null;

var panelIndex = 1;
startOver()

function plusPanels(n) {
  showPanels(panelIndex += n);
}

function showPanels(n) {
  var i;
  var x = document.getElementsByClassName("panel");
  console.log(x.length)
  if (n > x.length) {panelIndex = 1}    
  if (n < 1) {panelIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[panelIndex-1].style.display = "block";  
}

function startOver(){
	panelIndex = 1;
	showPanels(panelIndex);
}

function createBracket() {
  brackets = [];
  loserList = [];
  winnerList = [];
  elimationList = [];
  var choices = document.getElementById("list").value;
  var spliton = document.getElementById("spliter").value;
  choices = choices.split(spliton);

  console.log("Choices: " + choices);
  brackets = choices //TODO check to verify bracket is not empty
  console.log("Brackets: " + brackets);
  document.getElementById("Choice1").innerHTML = brackets[0];
  document.getElementById("Choice2").innerHTML = brackets[1];
  document.getElementById("next").disabled = true;
  plusPanels(1)
}

function Match(winner, loser){
	document.getElementById(winner).disabled = true;
	selectedButton = winner;
	document.getElementById(loser).disabled = false;
	notSelectedButton = loser;
	document.getElementById("next").disabled = false;
}


function NextRound(){
	winner = document.getElementById(selectedButton).innerHTML;
	loser = document.getElementById(notSelectedButton).innerHTML;
  brackets = remove(brackets,winner)
  brackets = remove(brackets,loser)

  if (loserList.includes(loser)){
    if (!elimationList.includes(loser)){
      elimationList.push(loser)
    }
  }else{
    loserList.push(loser)
    brackets.push(loser)
  }

  if (!winnerList.includes(winner)){
    winnerList.push(winner);
  }
  if (winnerList.length == 2){
    brackets.push(winnerList[0]);
    brackets.push(winnerList[1]);
    winnerList = [];
  }

  //Edge cases
  
  if (brackets.length == 1){
    if ((winnerList.length + brackets.length) == 2){
      brackets.push(winnerList[0]);
    } else{
      winnerList.push(brackets[0]);
    }
  }

  console.log("Brackets: " + brackets);
  console.log("Losers: " + loserList);
  console.log("Winners: " + winnerList);
  if (brackets.length !== 0){ //There is still more brackets to go
    document.getElementById("Choice1").innerHTML = brackets[0];
    document.getElementById("Choice2").innerHTML = brackets[1];
  } else{
    document.getElementById("Choice1").innerHTML = "Done";
    document.getElementById("Choice2").innerHTML = "Done";
    console.log("Winner is " + winnerList);
    console.log("Followed by:\n" + elimationList);
    createList(winner);
    plusPanels(1)
  }
	document.getElementById(selectedButton).disabled = false;
	document.getElementById(notSelectedButton).disabled = false;
	document.getElementById("next").disabled = true;
}

function createList(winner){
  document.getElementById("FinalList").innerHTML = ""
  var item = "<li>"+winner+"</li>";
  document.getElementById("FinalList").innerHTML += item;
  for (var i=0, l=elimationList.length; i<l; i++){
    var item = "<li>"+elimationList[(l-1) - i]+"</li>";
    document.getElementById("FinalList").innerHTML += item;
  }
}

function remove(list,element){
  var index = list.indexOf(element);
  if (index > -1) {
    list.splice(index, 1);
  }
  return list
}

document.addEventListener('input', function (event) {
  if (event.target.id.toLowerCase() !== 'list') return;
  autoExpand(event.target);
}, false);

document.addEventListener('input', function (event) {
  if (event.target.tagName.toLowerCase() !== 'textarea') return;
  search();
}, false);

var autoExpand = function (field) {

  // Reset field height
  field.style.height = 'inherit';

  // Get the computed styles for the element
  var computed = window.getComputedStyle(field);

  // Calculate the height
  var height = parseInt(computed.getPropertyValue('border-top-width'), 10)
               + parseInt(computed.getPropertyValue('padding-top'), 10)
               + field.scrollHeight
               + parseInt(computed.getPropertyValue('padding-bottom'), 10)
               + parseInt(computed.getPropertyValue('border-bottom-width'), 10);

  field.style.height = height + 'px';

};

function search(){

    var textareaValue = document.getElementById("list").value;
    
    var characterToSearch = document.getElementById("spliter").value;
    var count = 1;
    for(var i = 0; i < textareaValue.length; i++){
      if(characterToSearch == textareaValue[i]){
        count++;
      }
    }
    document.getElementById("numOfItems").innerHTML = "Number of items: " + count;
}